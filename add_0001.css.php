<?php
$namePalette = 'Forbidden_Ember';

$palette['hex'][0] = '#842DC6';
$palette['hex'][1] = '#BF2ADD';
$palette['hex'][2] = '#7225F7';

$palette['rgb'][0] = '132,45,198';
$palette['rgb'][1] = '191,42,221';
$palette['rgb'][2] = '114,37,247';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Cizme_Copii_in_anti'] = $palette;

$palette['hex'][0] = '#C9EFCE';
$palette['hex'][1] = '#D4CEE3';
$palette['hex'][2] = '#66DFB7';
$palette['hex'][3] = '#1BA779';
$palette['hex'][4] = '#70E482';

$palette['rgb'][0] = '201,239,206';
$palette['rgb'][1] = '212,206,227';
$palette['rgb'][2] = '102,223,183';
$palette['rgb'][3] = '27,167,121';
$palette['rgb'][4] = '112,228,130';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Hurricat_CLAD_1LP'] = $palette;

$palette['hex'][0] = '3C4259';
$palette['hex'][1] = 'EFB608';
$palette['hex'][2] = 'F4A30D';
$palette['hex'][3] = 'F29115';
$palette['hex'][4] = 'BE0407';

$palette['rgb'][0] = '60,66,89';
$palette['rgb'][1] = '239,182,8';
$palette['rgb'][2] = '244,163,13';
$palette['rgb'][3] = '242,145,21';
$palette['rgb'][4] = '190,4,7';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Forbidden_Ember'] = $palette;

$palette['hex'][0] = '#69D2E7';
$palette['hex'][1] = '#A7DBD8';
$palette['hex'][2] = '#E0E4CC';
$palette['hex'][3] = '#F38630';
$palette['hex'][4] = '#FA6900';

$palette['rgb'][0] = '105,210,231';
$palette['rgb'][1] = '167,219,216';
$palette['rgb'][2] = '224,228,204';
$palette['rgb'][3] = '243,134,48';
$palette['rgb'][4] = '250,105,0';

$palette['font']['hex'] = '#312F2F';
$palette['font']['rgb'] = '49, 47, 47';

$colorLover['palette']['Giant_Goldfish'] = $palette;

$palette['hex'][0] = '#ECD078';
$palette['hex'][1] = '#D95B43';
$palette['hex'][2] = '#C02942';
$palette['hex'][3] = '#542437';
$palette['hex'][4] = '#53777A';

$palette['rgb'][0] = '236,208,120';
$palette['rgb'][1] = '217,91,67';
$palette['rgb'][2] = '192,41,66';
$palette['rgb'][3] = '84,36,55';
$palette['rgb'][4] = '83,119,122';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Thought_Provoking'] = $palette;

$palette['hex'][0] = '#351330';
$palette['hex'][1] = '#424254';
$palette['hex'][2] = '#64908A';
$palette['hex'][3] = '#E8CAA4';
$palette['hex'][4] = '#CC2A41';

$palette['rgb'][0] = '53,19,48';
$palette['rgb'][1] = '66,66,84';
$palette['rgb'][2] = '100,144,138';
$palette['rgb'][3] = '232,202,164';
$palette['rgb'][4] = '204,42,65';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['you_are_beautiful'] = $palette;
?>

body {
  padding: 50px 50px 50px 50px;
}

.main {
  position: relative;
  width: 600px;
  height: 450px;
  background: url('background-random.php') no-repeat center center;
}

h1 {
    width: 100%;
    background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][0]; ?>);
    margin: 0 0 0 0;
    line-height: 60px;
    font-family: 'Titillium Web', sans-serif;
    color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;
}

h2 {
    position: absolute;
    width: 100%;
    background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][0]; ?>);
    top: 405px;
    
    margin: 0 0 0 0;
    line-height: 45px;
    font-family: 'Titillium Web', sans-serif;
    color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;
    text-align: center;
}

.phone {
  background: url('phone-symbol-2.png') rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][1]; ?>) no-repeat 420px 0px;
  line-height: 32px;
  text-align: right;
  color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;  
  
  font-weight: bolder;
  padding-right: 20px;
  font-family: 'Titillium Web', sans-serif;
}

.quote {
  position: absolute;
  left: 490px;
  top: 15px;
  width: 90px;
  height: 90px;
  background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][2]; ?>);
  opacity: 0.9;
  filter: alpha(opacity=90);
  padding-top: 10px;  
  padding-left: 15px;
  text-align: center;
  border-radius: 50px;
} 

.quote p {
  width: 65px;
  font-family: 'Montserrat', sans-serif;  
}

.imageLogo {
    position: absolute;
    top: 50px;
    left: 50px;
}

ul {
  
  position: relative;
  float: right;
  
  list-style-type: none;
  padding-left: 5px;
}

ul li {
    color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;
    font-family: 'Titillium Web',sans-serif;
    font-weight: bolder;
    margin: 5px;
    
    border-radius: 30px;
    text-align: center;
    padding: 10px 10px 10px 10px;
    line-height: 30px;
}

<?php 
  /** for ($i = 0; $i < count($colorLover['palette'][$namePalette]['hex']); $i++): */
  for ($i = 0; $i < 100; $i++): 
?>
  ul li.li_<?php echo $i; ?> {
    /** width: <?php echo 100+($i*50);?>px; */
    background-color: <?php echo $colorLover['palette'][$namePalette]['hex'][$i % count($colorLover['palette'][$namePalette]['hex'])]; ?>; 
  }
<?php endfor; ?>
