<?php
require 'palette.php';

$namePalette = 'Forbidden_Ember';


?>

body {
  padding: 50px 50px 50px 50px;
}

.main {
  position: relative;
  width: 600px;
  height: 450px;
  background: url('background-random.php') no-repeat top center;

}

h1 {
    width: 100%;
    background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][0]; ?>);
    margin: 0 0 0 0;
    line-height: 60px;
    font-family: 'Righteous', cursive;
    color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;
}

h2 {
    position: absolute;
    width: 100%;
    background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][0]; ?>);
    top: 405px;
    
    margin: 0 0 0 0;
    line-height: 45px;
    font-family: Verdana, Geneva, sans-serif;
    font-size: 12px;
    font-weight: normal;
    color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;
    text-align: center;
}

.slogan {
  position: relative;
  top: 275px;
  
  background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][4]; ?>);
  padding: 5px 5px 5px 5px;
  
  text-align: center;
  font-family: 'Righteous', cursive;
  font-size: 16px;
  
  color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>; 
  opacity: 0.9;
}

.phone {
  position: absolute;
  top: 5px;
  left: 430px;
  
  padding: 10px 10px 10px 10px;
  
  background: url('phone-symbol-1.png') rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][1]; ?>) no-repeat 5px 5px;
  width: 145px;
  text-align: right;
  color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;  
  
  font-weight: bolder;
  font-family: 'Righteous', cursive;
}

.quote {
  position: absolute;
  left: 490px;
  top: 15px;
  width: 90px;
  height: 90px;
  background-color: rgb(<?php echo $colorLover['palette'][$namePalette]['rgb'][2]; ?>);
  opacity: 0.9;
  filter: alpha(opacity=90);
  padding-top: 10px;  
  padding-left: 15px;
  text-align: center;
  border-radius: 50px;
} 

.quote p {
  width: 65px;
  font-family: 'Righteous', cursive;
}

.imageLogo {
    position: absolute; 
    top: 50px;
    left: 250px;
    
    
    display: block;

    
    width: 160px;
    height: 160px;
    border-radius:80px;
    
    text-align: center;
   
    background-color: #ffffff;
    
    opacity: 0.8;
}

.imageLogo img {
  margin-top: 50px;
}

ul {
  
  position: absolute;
  
  top: 310px;
  list-style-type: none;
  padding: 0px 0px 0px 0px;
  
  text-align: center;
}

ul li {
  position: relative;
  float: left;
  
    width: 170px;
  
    color: <?php echo $colorLover['palette'][$namePalette]['font']['hex']; ?>;
    font-family: 'Titillium Web',sans-serif;
    font-weight: bolder;
    margin-bottom: 5px;
    
    text-align: right;
    padding-right: 5px;
    line-height: 30px;
    

      margin-right: 5px;
}



<?php 
  /** for ($i = 0; $i < count($colorLover['palette'][$namePalette]['hex']); $i++): */
  for ($i = 0; $i < 100; $i++): 
?>
  ul li.li_<?php echo $i; ?> {
    background-color: <?php echo $colorLover['palette'][$namePalette]['hex'][$i % count($colorLover['palette'][$namePalette]['hex'])]; ?>;
    opacity: 0.9;
  }
<?php endfor; ?>
