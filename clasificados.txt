Welcome to Sophisticated Sites. We are an Athens, Georgia based web design company serving up awesome websites to the world. Don’t settle for a template website. We will create a custom design to make your website stand out from the rest of the crowd. 

Sophisticated Sites is currently working on new websites for Oconee Properties, Inc. of Watkinsville, Georgia and Memory Lane Inn with locations in Tyler, Texas and McKinney, Texas. 

