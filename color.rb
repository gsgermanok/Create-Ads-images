#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'

hex = Hash.new
r, g, b = Hash.new 

def getColorHex(colors)
  return '#000000'
end

def getColorR(colors)
  return 255
end

def getColorG(colors)
  return 255
end

def getColorB(colors)
  return 255
end


def urlParse(url)
  colourlovers = Nokogiri::HTML(open(url))
  
  colourlovers.css("div.col-70 h4").each do |color|
    puts color
  end
  
  colourlovers.css("div.col-80 h4").each do |color|
    r = {color.to_s.split("\,")[0].gsub('<h4>')}
    g = {color.to_s.split("\,")[1]}
    b = {color.to_s.split("\,")[2].gsub('</h4>')}
  end
  
  puts r, g, b
end  

ARGV.each do |url|
  urlParse(url)
end